---
title: "Cheatsheet"
---
# Dynamic Routing
# Address Translation
# IPv6
# Automatic IP Addresses
## Bootstrap Protocol
## DHCPv4
The client uses 0.0.0.0 as the source address if he doesn't know his own
address and 255.255.255.255 as the destination address if he doesn't know the
address of the DHCP server.

The client either receives an address from an pool of available addresses or he
gets assigned an address based on his MAC address.

The packet structure is almost the same as BOOTP with the difference that one
can add options of variable length. The options are input in TLV format.

![DHCP](assets/dhcp.png)

## DHCPv6